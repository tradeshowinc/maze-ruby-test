# Maze Ruby Test

Given the following maze

```
******************************************
*               ******            ***    *
*   ***   ***   ******      * B   ***    *
*   ***   ***   ******     ***           *
*   ***                   *****   ***    *
*   *********             *****   ***    *
*   *******      ****      ***    ***    *
*               ******                   *
*     ***        ****       **********   *
*     ***                  ***********   *
*     *******             ************   *
*     *******    ****                    *
*      A         ****               ******
******************************************
```

How would we create instructions in Ruby for the shortest path from A to B?

*Notes*:

* Top left corner is point (0, 0).
* The code should receive a maze string and output an array of point movements. For example, the response should be `[[7,12], [8,12], ...]`